
// noinspection JSUnresolvedVariable

export const UWP = {};

const SECTOR_INDEXES = 'abcdefghijklmnop'.split('');

UWP.routeStart = null;
UWP.routeEnd = null;

// World code tables taken from Traveller Map:
// https://github.com/inexorabletash/travellermap/blob/5071401455137dfb36596427a1378ee746a3dae6/world_util.js
UWP.ATMOSPHERE = {
    0: 'No atmosphere',
    1: 'Trace',
    2: 'Very thin; Tainted',
    3: 'Very thin',
    4: 'Thin; Tainted',
    5: 'Thin',
    6: 'Standard',
    7: 'Standard; Tainted',
    8: 'Dense',
    9: 'Dense; Tainted',
    A: 'Exotic',
    B: 'Corrosive',
    C: 'Insidious',
    D: 'Dense, high',
    E: 'Thin, low',
    F: 'Unusual',
    X: 'Unknown',
    '?': 'Unknown'
}

UWP.GOVERNMENT = {
    0: 'No Government Structure',
    1: 'Company/Corporation',
    2: 'Participating Democracy',
    3: 'Self-Perpetuating Oligarchy',
    4: 'Representative Democracy',
    5: 'Feudal Technocracy',
    6: 'Captive Government / Colony',
    7: 'Balkanization',
    8: 'Civil Service Bureaucracy',
    9: 'Impersonal Bureaucracy',
    A: 'Charismatic Dictator',
    B: 'Non-Charismatic Dictator',
    C: 'Charismatic Oligarchy',
    D: 'Religious Dictatorship',
    E: 'Religious Autocracy',
    F: 'Totalitarian Oligarchy',
    G: 'Small Station or Facility',
    H: 'Split Clan Control',
    J: 'Single On-world Clan Control',
    K: 'Single Multi-world Clan Control',
    L: 'Major Clan Control',
    M: 'Vassal Clan Control',
    N: 'Major Vassal Clan Control',
    P: 'Small Station or Facility',
    Q: 'Krurruna or Krumanak Rule for Off-world Steppelord',
    R: 'Steppelord On-world Rule',
    S: 'Sept',
    T: 'Unsupervised Anarchy',
    U: 'Supervised Anarchy',
    W: 'Committee',
    X: 'Unknown',
    '?': 'Unknown'
};

UWP.LAW = {
    0: 'No prohibitions',
    1: 'Body pistols, explosives, and poison gas prohibited',
    2: 'Portable energy weapons prohibited',
    3: 'Machine guns, automatic rifles prohibited',
    4: 'Light assault weapons prohibited',
    5: 'Personal concealable weapons prohibited',
    6: 'All firearms except shotguns prohibited',
    7: 'Shotguns prohibited',
    8: 'Long bladed weapons controlled; open possession prohibited',
    9: 'Possession of weapons outside the home prohibited',
    A: 'Weapon possession prohibited',
    B: 'Rigid control of civilian movement',
    C: 'Unrestricted invasion of privacy',
    D: 'Paramilitary law enforcement',
    E: 'Full-fledged police state',
    F: 'All facets of daily life regularly legislated and controlled',
    G: 'Severe punishment for petty infractions',
    H: 'Legalized oppressive practices',
    J: 'Routinely oppressive and restrictive',
    K: 'Excessively oppressive and restrictive',
    L: 'Totally oppressive and restrictive',
    S: 'Special/Variable situation',
    X: 'Unknown',
    '?': 'Unknown'
};

UWP.TECH = {
    0: 'Stone Age',
    1: 'Bronze, Iron',
    2: 'Printing Press',
    3: 'Basic Science',
    4: 'External Combustion',
    5: 'Mass Production',
    6: 'Nuclear Power',
    7: 'Miniaturized Electronics',
    8: 'Quality Computers',
    9: 'Anti-Gravity',
    A: 'Interstellar community',
    B: 'Lower Average Imperial',
    C: 'Average Imperial',
    D: 'Above Average Imperial',
    E: 'Above Average Imperial',
    F: 'Technical Imperial Maximum',
    G: 'Robots',
    H: 'Artificial Intelligence',
    J: 'Personal Disintegrators',
    K: 'Plastic Metals',
    L: 'Comprehensible only as technological magic',
    X: 'Unknown',
    '?': 'Unknown'
};



UWP.printNumber = function (number, precision) {
    if (number === 0) {
        return 0;
    }
    number = parseFloat(number);
    if (precision === null || precision === undefined || precision < 0) {
        precision = 2;
    }

    if (number > 1e12 || number < 1e-3) {
        return number.toExponential(precision);
    } else if (number > 99) {
        return Number(parseInt(number)).toLocaleString();
    } else {
        return number.toPrecision(precision);
    }
};


UWP.help = function(chatData) {
    let text = `<div class="uwp">`;
    text += `<h2>Traveller Map Tools</h2>`;

    text += `/uwp &lt;world name><br/>`;
    text += `/uwp &lt;sector> &lt;xxyy><br/>`;
    text += `/route [&lt;jump>]<br/><br/>`;

    text += `</div>`;

    chatData.content = text;
    ChatMessage.create(chatData);
};

UWP.getText = function(code, table) {
    if (code) {
        if (table[code]) {
            return `(${code}) ${table[code]}`;
        } else {
            return `(${code}) ?`;
        }
    } else {
        return "(?)";
    }
}

UWP.getAtmosphereText = function(code) {
    return UWP.getText(code, UWP.ATMOSPHERE);
};

UWP.getGovernmentText = function(code) {
    return UWP.getText(code, UWP.GOVERNMENT);
};

UWP.getLawText = function(code) {
    return UWP.getText(code, UWP.LAW);
}

UWP.getTechText = function(code) {
    return UWP.getText(code, UWP.TECH);
}

UWP.uwpToText = function(uwp, pbg) {
    let text = "<div>";

    let starport = uwp.substring(0, 1);
    let size = parseInt(uwp.substring(1, 2), 16) * 1600;
    let atmosphere = uwp.substring(2, 3);
    let hydrographics = parseInt(uwp.substring(3, 4), 16);
    let population = parseInt(uwp.substring(4, 5), 16);
    let government = uwp.substring(5, 6);
    let law = uwp.substring(6, 7);
    let tech = uwp.substring(8, 9);

    let popDigit = parseInt(pbg.substring(0, 1), 16);
    if (population > 0) {
        population = parseInt(popDigit * Math.pow(10, population));
    } else {
        population = 0;
    }

    text += "<dl class='traveller-map-uwp'>";
    text += `<dt>Starport</dt><dd>${starport}</dd>`;
    text += `<dt>Size</dt><dd>${UWP.printNumber(size)}km</dd>`;
    text += `<dt>Atmosphere</dt><dd>${UWP.getAtmosphereText(atmosphere)}</dd>`;
    text += `<dt>Hydrographics</dt><dd>${hydrographics * 10}%</dd>`;
    text += `<dt>Population</dt><dd>${UWP.printNumber(population)}</dd>`;
    text += `<dt>Government</dt><dd>${UWP.getGovernmentText(government)}</dd>`;
    text += `<dt>Law Level</dt><dd>${UWP.getLawText(law)}</dd>`;
    text += `<dt>Tech Level</dt><dd>${UWP.getTechText(tech)}</dd>`;
    text += "</dl>";
    text += "</div>";
    return text;
}

UWP.uwpCommand = function(chatData, args) {
    if (args < 1) {
        return;
    }

    if (args.length === 2 && args[0].length === 4 && args[1].length === 4) {
        // Sector and sector coord
        let sector = args.shift();
        let xy = args.shift();

        UWP.displayHex(sector, xy);
    } else {
        let query = ""
        while (args.length > 0) {
            if (query.length > 0) {
                query += " ";
            }
            query += args.shift();
        }
        UWP.displaySearch(query);
    }
}

UWP.displaySearch = function(query) {
    let HOST = game.settings.get('traveller-map', 'mapUrl');
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", `${HOST}/api/search?q=${escape(query)}`);

    xmlHttp.onload = function () {
        if (xmlHttp.status === 200) {
            let obj = JSON.parse(xmlHttp.responseText);
            UWP.processSearchResults(obj);
        } else {
            ui.notifications.error("Unable to connect to TravellerMap");
        }
    }

    xmlHttp.onerror = function () {
        ui.notifications.error("Error connecting to TravellerMap");
    }
    xmlHttp.send();
}

UWP.chatMessage = function(text) {
    let whisper = game.settings.get('traveller-map', 'whisper')?[ game.userId ]: [ ];
    let chatData = {
        user: game.userId,
        speaker: ChatMessage.getSpeaker(),
        content: text,
        whisper: whisper
    }
    ChatMessage.create(chatData, {});
}

UWP.processSearchResults = function(obj) {
    if (obj.Results && obj.Results.Count > 0) {
        // If we have exactly one world, display that, otherwise display a list.
        // Some of the 'hits' can be sectors, so we might get > 1 result, but only
        // one is a world. It's probably always going to be the first entry, but
        // we allow for it not to be.
        let world = -1;
        for (let w=0; w < obj.Results.Items.length; w++) {
            if (obj.Results.Items[w].World) {
                if (world != -1) {
                    world = -1;
                    break;
                } else {
                    world = w;
                }
            }
        }
        if (world > -1) {
            let item = obj.Results.Items[world];
            let sector = escape(item.World.Sector);
            let x = (""+item.World.HexX).padStart(2, "0");
            let y = (""+item.World.HexY).padStart(2, "0");
            UWP.displayHex(sector, `${x}${y}`);
        } else if (obj.Results.Items) {
            let text = "<div class='uwp'><ul>";
            let max = game.settings.get('traveller-map', 'maxResults');
            for (let item of obj.Results.Items) {
                if (item.World) {
                    let sector = escape(item.World.Sector);
                    let x = (""+item.World.HexX).padStart(2, "0");
                    let y = (""+item.World.HexY).padStart(2, "0");

                    text += `<li class="world-item" data-sector="${sector}" data-hex="${x}${y}"><a>${item.World.Sector} / ${item.World.Name} (${x}${y})</a></li>`;
                    if (--max < 1) {
                        break;
                    }
                }
            }
            text += "</ul></div>";

            UWP.chatMessage(text);
        }
    } else {
        ui.notifications.warn("Unable to find any worlds matching that search");
        return;
    }
}

UWP.displayHex = function(sector, xy, func) {
    let HOST = game.settings.get('traveller-map', 'mapUrl');
    let xmlHttp = new XMLHttpRequest();

    xmlHttp.open("GET", `${HOST}/data/${sector}/${xy}`, true);
    xmlHttp.onload = function () {
        if (xmlHttp.status === 200) {
            let obj = JSON.parse(xmlHttp.responseText);
            if (func) {
                func(obj, sector, xy);
            } else {
                UWP.processHexResults(obj, sector, xy);
            }
        } else {
            ui.notifications.error("Unable to connect to TravellerMap");
        }
    }

    xmlHttp.onerror = function () {
        ui.notifications.error("Error connecting to TravellerMap");
    }
    xmlHttp.send();

}

UWP.processHexResults = function(obj, sector, xy) {
    if (obj.Worlds.length == 0) {
        ui.notifications.warn("No world found at location ${xy}");
        return;
    }

    let data = obj.Worlds[0];
    let radius = game.settings.get('traveller-map', 'miniRadius');
    let HOST = game.settings.get('traveller-map', 'mapUrl');
    let style = game.settings.get('traveller-map', 'style');
    let hex = game.settings.get('traveller-map', 'hexNumbers')?"&allhexes=1":"";

    let mapUrl = `${HOST}/api/jumpmap?sector=${data.Sector}&hex=${data.Hex}&style=${style}&options=49392${hex}`;
    let fullUrl = `${HOST}/?sector=${data.Sector}&hex=${data.Hex}&style=${style}&options=58367${hex}`;

    let text = `<div class="uwp"><div class="uwp-text">`;
    text += `<h2>${data.Name}</h2>`;
    text += `<span class="subsector">${data.SubsectorName} Subsector</span><br/>`;
    text += `<span class="sector-name">${data.Sector} ${data.Hex}</span><br/>`;
    text += `<span class="allegiance">${data.AllegianceName}</span><br/><br/>`;
    text += `${data.UWP}<br/>`;
    text += `${data.Remarks}<br/>`;
    let uwp = data.UWP;
    text += UWP.uwpToText(uwp, data.PBG);
    text += "</div>";
    if (radius > 0) {
        text += `<img class="mini-jump-map" src='${mapUrl}&jump=${radius}' title="${data.Name}" data-url="${fullUrl}"/>`;
    } else {
        text += `<a class="mini-jump-map" title="${data.Name}" data-url="${fullUrl}">View on map</a>`;
    }
    text += "<ul class='jump-links'>";
    for (let j=1; j <= 6; j++) {
        text += `<li><a class="mini-jump-map" title="${data.Name}" data-jump="${j}" data-url="${mapUrl}">J-${j}</a></li>`;
    }
    text += "</ul>";

    text += `<a class="centre-link uwp-route-add" data-sector="${data.SectorAbbreviation}" \ 
            data-name="${data.Name}" data-xy="${data.Hex}" \
            title="Use /route to calculate a route">Add to route</a>`;

    //text += `<a class='uwp-journal' data-sector='${data.Sector}' data-name="${data.Name}" data-xy="${xy}">Create Journal</a>`;
    text += "</div>";

    UWP.chatMessage(text);
}

UWP.routeCommand = function(chatData, args) {
    let jump = parseInt(game.settings.get('traveller-map', 'defaultJump'));
    if (args.length > 0) {
        jump = parseInt(args.shift())
    }

    if (!UWP.routeStart || !UWP.routeEnd) {
        ui.notifications.error("You need to first specify a start and destination for your route");
        return;
    }
    let startSec = UWP.routeStart.replaceAll(/ .*/g, "");
    let startHex = UWP.routeStart.replaceAll(/.* /g, "");
    let endSec = UWP.routeEnd.replaceAll(/ .*/g, "");
    let endHex = UWP.routeEnd.replaceAll(/.* /g, "");

    const HOST = game.settings.get('traveller-map', 'mapUrl');
    let xmlHttp = new XMLHttpRequest();

    const url = `${HOST}/api/route?start=${startSec}+${startHex}&end=${endSec}+${endHex}&jump=${jump}`;

    xmlHttp.open("GET", url, true);
    xmlHttp.onload = function () {
        if (xmlHttp.status === 200) {
            let obj = JSON.parse(xmlHttp.responseText);
            UWP.processRouteResults(obj, jump);
        } else {
            ui.notifications.error("Unable to find route");
        }
    }

    xmlHttp.onerror = function () {
        ui.notifications.error("Error connecting to TravellerMap");
    }
    xmlHttp.send();
}

UWP.processRouteResults = function(obj, jump) {
    console.log(obj);
    let text = "<div class='uwp'>";

    let start = obj[0];
    let end = obj[obj.length -1];

    text += `<h2>J-${jump} Route</h2>`;
    text += `<p>From ${start.Name} ${start.Hex}/${start.Subsector}/${start.Sector} `;
    text += `to ${end.Name} ${end.Hex}/${end.Subsector}/${end.Sector}</p>`;

    text += "<ul>";
    //    /route troj 2616 troj 3215 2
    let route = { "Tour": true, "Route": true, "Results": { Items: [] }};
    for (let i=0; i < obj.length; i++) {
        let world = obj[i];
        text += `<li>${world.Name} ${world.Hex}</li>`;

        let o = { "Name": obj[i].Name, "SectorX": obj[i].SectorX, "SectorY": obj[i].SectorY, "HexX": obj[i].HexX, "HexY": obj[i].HexY };
        let w = { World: o };
        route.Results.Items.push(w);
    }
    text += "</ul>";
    console.log(route);
    let url = "https://travellermap.com/?qr=" + encodeURIComponent(JSON.stringify(route));
    if (url.length > 2048) {
        text += `<p><i>Route too long for map</i></p>`;
    } else {
        text += `<a class="centre-link mini-jump-map" title="Route" data-url="${url}">View on map</a>`;
    }
    text += "</div>";
    UWP.chatMessage(text);
}

UWP.findSubsectors = async (name) => {
    try {
        const server = game.settings.get('traveller-map', 'mapUrl');
        const url = `${server}/api/search?q=${name}`;

        const response = await fetch(url);
        const result = await response.json();
        return result.Results.Items.filter((item) => 'Subsector' in item);
    } catch(err) {
        console.error(err);
        ui.notifications.error("Unable to connect to TravellerMap");
    }
}

UWP.findSectors = async (name) => {
    try {
        const server = game.settings.get('traveller-map', 'mapUrl');
        const url = `${server}/api/search?q=${encodeURIComponent(name)}`;

        const response = await fetch(url);
        const result = await response.json();
        return result.Results.Items.filter((item) => 'Sector' in item);
    } catch(err) {
        console.error(err);
        ui.notifications.error("Unable to connect to TravellerMap");
    }
}

UWP.isSectorSubsector = (args) => {
    return args.length > 1 && SECTOR_INDEXES.includes(args[args.length-1].toLowerCase());
}

UWP.subsectorCommand = async function(chatData, args) {
    let subsectors;
    if (UWP.isSectorSubsector(args)) {
        const subsectorIndex = args[args.length-1].toLowerCase();
        const sectorName = args.slice(0, -1).join(' ');
        const sectors = await UWP.findSectors(sectorName);
        if (sectors.length === 0) {
            UWP.chatMessage(`No sector found matching ${sectorName}`);
        } else if (sectors.length > 1) {
            const data = {sectors: sectors, index: subsectorIndex};
            const sectorList = await renderTemplate('modules/traveller-map/templates/sectors.hbs', data);
            UWP.chatMessage(sectorList);
        } else
            UWP.displaySubsector(sectors[0].Sector.Name, subsectorIndex, subsectorIndex);
    } else {
        subsectors = await UWP.findSubsectors(args.join(' '));

        if (subsectors.length === 0) {
            UWP.chatMessage(`No subsector found matching ${args.join(' ')}`);
        } else if (subsectors.length > 1) {
            const data = {subsectors: subsectors};
            const subsectorList = await renderTemplate('modules/traveller-map/templates/subsectors.hbs', data);
            UWP.chatMessage(subsectorList);
        } else
            UWP.displaySubsector(subsectors[0].Subsector.Sector, subsectors[0].Subsector.Name, subsectors[0].Subsector.Index);
    }
}

UWP.displaySubsector = (sector, name, index) => {
    try {
        const server = game.settings.get('traveller-map', 'mapUrl');
        const style = game.settings.get('traveller-map', 'style');
        let mapUrl = `${server}/data/${encodeURIComponent(sector)}/${index}/image?style=${style}`;
        const ip = new ImagePopout(mapUrl, {title: `${sector} / ${name}`, shareable: true});
        ip.render(true);
    } catch(err) {
        console.error(err);
    }
}
