#!/bin/bash

version=$(cat traveller-map/module.json | jq -r .version)
patch=$(echo $version | sed 's/.*\.//')
minor=$(echo $version | sed 's/\.[0-9]*$//')

if [ -z $1 ]
then
    patch=$((patch + 1))
fi

version=${minor}.${patch}
echo $version

sed -i "s/\"version\": \".*\",/\"version\": \"$version\",/" traveller-map/module.json
sed -i "s#/raw/v[0-9.]*/#/raw/v${version}/#" traveller-map/module.json

zip -r release/traveller-map.zip ./traveller-map
mkdir -p release
cp traveller-map/module.json release/module.json
