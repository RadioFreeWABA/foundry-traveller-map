import { UWP } from "./uwp.mjs";
import { JOURNAL } from "./journal.mjs";

Hooks.once('init', async function() {
    game.settings.register('traveller-map', 'mapUrl', {
        name: 'Base URL for the map service',
        hint: "This shouldn't need to change, unless you are hosting your own service.",
        scope: 'world',
        config: true,
        type: String,
        default: "https://travellermap.com"
    });
    game.settings.register('traveller-map', 'miniRadius', {
        name: 'Size of inline jump map',
        hint: 'Number of parsecs radius to show for the inline-chat jump map. Clicking on this opens an inline browser window showing the local region.  Set to zero to show a text link instead.',
        scope: 'world.',
        config: true,
        type: Number,
        default: 2
    });
    game.settings.register('traveller-map', 'maxResults', {
        name: 'Maximum number of search results',
        hint: 'When searching for systems, maximum number to show in the results',
        scope: 'world',
        config: true,
        type: Number,
        default: 10
    });
    /*
    game.settings.register('traveller-map', 'journalFolder', {
        name: 'Journal folder',
        hint: 'Folder to put journal entries in',
        scope: 'world',
        config: true,
        type: String,
        default: "Worlds"
    });
     */
    game.settings.register('traveller-map', 'whisper', {
        name: 'Whisper results to self',
        hint: 'If true, results are whispered to the user. Otherwise they are broadcast to everyone',
        scope: 'client',
        config: true,
        type: Boolean,
        default: true
    });
    game.settings.register('traveller-map', 'style', {
        name: 'Map style',
        hint: 'Select the style of the map to use when displaying jump maps.',
        scope: 'client',
        config: true,
        type: String,
        choices: {
            "poster": "Default Deluxe Traveller poster style",
            "print": "Print style with white background",
            "atlas": "Atlas of the Imperium style",
            "candy": "Interactive map style",
            "draft": "GDW's original draft style",
            "fasa": "Classic FASA supplements style",
            "terminal": "Retro-futuristic look",
            "mongoose": "Mongoose Publishing style"
        },
        default: "poster"
    });
    game.settings.register('traveller-map', 'hexNumbers', {
        name: 'Show hex numbers',
        hint: 'Always show hex numbers, even in empty hexes.',
        scope: 'client',
        config: true,
        type: Boolean,
        default: false
    });
    game.settings.register('traveller-map', 'mapSize', {
        name: 'Map size',
        hint: 'Window size for the inline browser used to display the map.',
        scope: 'client',
        config: true,
        type: Number,
        choices: {
            "300": "Tiny",
            "420": "Small",
            "600": "Medium",
            "900": "Large"
        },
        default: 600
    });
    game.settings.register('traveller-map', 'mapOrientation', {
        name: 'Map orientation',
        hint: 'Whether to show the browser as portrait, landscape or square.',
        scope: 'client',
        config: true,
        type: String,
        choices: {
            "portrait": "Portrait",
            "landscape": "Landscape",
            "square": "Square",
        },
        default: "landscape"

    });
    game.settings.register('traveller-map', 'defaultJump', {
        name: 'Default jump rating',
        hint: 'Default jump rating to use when plotting a route.',
        scope: 'client',
        config: true,
        type: Number,
        choices: {
            "1": "Jump-1",
            "2": "Jump-2",
            "3": "Jump-3",
            "4": "Jump-4",
            "5": "Jump-5",
            "6": "Jump-6"
        },
        default: 2
    });
});

Hooks.on('renderChatMessage', function(app, html) {
    let links = html.find(".mini-jump-map");
    for (let l=0; l < links.length; l++) {
        let mini = links[l];
        if (mini) {
            let radius = 3;
            let src = mini.getAttribute("data-url");
            let title = mini.getAttribute("title");
            if (mini.getAttribute("data-jump")) {
                radius = parseInt(mini.getAttribute("data-jump"));
                mini.addEventListener("click", ev => {
                    const ip = new ImagePopout(src + "&jump=" + radius, {
                        title: title,
                        shareable: true
                    });
                    ip.render(true);
                });
            } else {
                let height = parseInt(game.settings.get("traveller-map", "mapSize"));
                let width = height;
                if (game.settings.get("traveller-map", "mapOrientation") === "landscape") {
                    width = (height / 3) * 4;
                } else if (game.settings.get("traveller-map", "mapOrientation") === "portrait") {
                    height = (width / 3) * 4;
                }
                mini.addEventListener("click", ev => {
                    Ardittristan.InlineViewer.sendUrl(src, false, width, height, title, "", [game.user.id], {});
                });
            }
        }
    }
    let items = html.find(".world-item");
    for (let i=0; i < items.length; i++) {
        let item = items[i];
        if (item) {
            let sector = item.getAttribute("data-sector");
            let hex = item.getAttribute("data-hex");

            item.addEventListener("click", ev => {
                UWP.displayHex(sector, hex);
            });
        }
    }
    let j = html.find(".uwp-journal")[0];
    if (j) {
        j.addEventListener("click", ev => {
            JOURNAL.createJournal(j, html);
        })
    }
    let r = html.find(".uwp-route-add")[0];
    if (r) {
        r.addEventListener("click", ev => {
            let name = r.getAttribute("data-name");
            let hex = r.getAttribute("data-xy");
            let sector = r.getAttribute("data-sector");
            if (!UWP.routeStart) {
                UWP.routeStart = `${sector} ${hex}`;
                ui.notifications.info(`Added ${name} ${hex} to start of route`);
            } else if (!UWP.routeEnd) {
                UWP.routeEnd = `${sector} ${hex}`;
                ui.notifications.info(`Added ${name} ${hex} to end of route`);
            } else {
                UWP.routeStart = UWP.routeEnd;
                UWP.routeEnd = `${sector} ${hex}`;
                ui.notifications.info(`Added ${name} ${hex} to end of route`);
            }
        })
    }

    // hook subsector entries to clicking on them renders the subsector map
    const subsectors = html.find(".subsector-item");
    for (const subsector of subsectors) {
        const sector = subsector.getAttribute("data-sector");
        const name = subsector.getAttribute("data-name");
        const index = subsector.getAttribute("data-index");
        subsector.addEventListener("click", async (ev) => {
            await UWP.displaySubsector(sector, name, index);
        });
    }
});


Hooks.on("chatMessage", function(chatlog, message, chatData) {
    if (message.indexOf("/uwp") === 0) {
        let args = message.split(" ");
        args.shift();
        if (args.length === 0) {
            UWP.help(chatData);
            return false;
        }
        UWP.uwpCommand(chatData, args);
        return false;
    } else if (message.indexOf("/route") === 0) {
        let args = message.split(" ");
        args.shift();
        UWP.routeCommand(chatData, args);
        return false;
    } else if (message.indexOf("/subsector") === 0) {
        let args = message.split(" ");
        args.shift();
        UWP.subsectorCommand(chatData, args);
        return false;
    }
});

