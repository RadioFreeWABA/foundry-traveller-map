import { UWP } from "./uwp.mjs";

export const JOURNAL = {};

JOURNAL.getFolder = function(sectorName, create) {
    let folders = game.journal.directory.folders;
    console.log("Journal:");
    let folder = null;
    for (let i=0; i < folders.length; i++) {
        console.log("folder: " + folders[i].data.name);
        if (folders[i].data.name === "Worlds") {
            folder = folders[i];
            break;
        }
    }
    if (folder == null) {
        return null;
    }

    console.log(folder);
    let sectors = folder.getSubfolders();
    let sector = null;
    for (let s=0; s < sectors.length; s++) {
        console.log(sectors[s]);
        if (sectors[s].data.name === sectorName) {
            sector = sectors[s];
            break;
        }
    }
    if (!sector && create) {
        let folderData = {
            "name": sectorName,
            "parent": folder.data._id,
            "type": "JournalEntry"
        }
        Folder.create(folderData);
        return null;
    } else if (!sector) {
        return null;
    }
    return sector;
}

JOURNAL.createJournal = function(j, html) {
    let sectorName = j.getAttribute("data-sector");
    let worldName = j.getAttribute("data-name");
    let worldXY = j.getAttribute("data-xy");

    console.log(sectorName);
    console.log(worldName);
    console.log(html.find(".uwp-text")[0]);

    let folder = JOURNAL.getFolder(sectorName, true);
    UWP.displayHex(sectorName, worldXY, JOURNAL.createNewEntry);
};

JOURNAL.createNewEntry = function(obj, sectorName, worldXY) {
    let folder = JOURNAL.getFolder(sectorName, false);

    if (folder === null) {
        console.log("No such folder");
        return;
    }
    console.log("JOURNAL.createNewEntry:");
    console.log(obj.Worlds);

    if (obj.Worlds && obj.Worlds.length == 1) {
        let world = obj.Worlds[0];
        let text = `<h1>${world.Name} (${world.Hex})</h1>`;
        text += UWP.uwpToText(world.UWP, world.PBG);
        let data = {
            "name": `${worldXY} ${world.Name}`,
            "content": text,
            "folder": folder.data._id
        }
        JournalEntry.create(data);
    }
    /*
    let data = {
        "name": `${worldXY} ${worldName}`,
        "content": html.find(".uwp-text")[0].toString(),
        "folder": folder.data._id
    }

     */

}